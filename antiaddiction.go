package antiaddiction

import (
	"bytes"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	// 实名认证接口地址
	checkURL = "https://api.wlc.nppa.gov.cn/idcard/authentication/check"
	// 实名认证结果查询接口地址
	queryURL = "http://api2.wlc.nppa.gov.cn/idcard/authentication/query"
	// 游戏用户行为数据上报接口地址
	loginoutURL = "http://api2.wlc.nppa.gov.cn/behavior/collection/loginout"
)

type Error struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

/**
 * @title: NewAnti
 * @description: "初始化网络游戏防沉迷实名认证系统系统参数"
 * @param {string} appID "接口调用唯一凭证"
 * @param {string} secretKey "访问密钥"
 * @param {string} bizID "业务权限标识"
 * @return {*AntiConfig}
 */
func NewAnti(appID, secretKey, bizID string) *AntiConfig {
	return &AntiConfig{
		AppID:     appID,
		SecretKey: secretKey,
		BizID:     bizID,
	}
}

type AntiConfig struct {
	AppID     string `json:"appId"`     // 接口调用唯一凭证，由系统发放
	SecretKey string `json:"secretKey"` // 访问密钥
	BizID     string `json:"bizId"`     // 业务权限标识，与游戏备案识别码一致
}

/**
 * @title: request
 * @description: "短链接请求接口"
 * @param {string} method "请求方式"
 * @param {string} url "请求地址"
 * @param {map[string]interface{}} data "请求参数"
 * @param {map[string]string} signData "计算签名参数"
 * @return {[]byte} "请求返回"
 * @return {error} "错误信息"
 */
func (ac *AntiConfig) request(method, url string, data map[string]interface{}, signData map[string]string) ([]byte, error) {
	byteData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	milliTime := strconv.FormatInt(time.Now().UnixMilli(), 10)
	if len(signData) == 0 {
		signData = make(map[string]string)
	}
	signData["appId"] = ac.AppID
	signData["bizId"] = ac.BizID
	signData["timestamps"] = milliTime
	bodyByte := []byte{}
	if len(data) > 0 {
		bodyData, err := aes128gcm(string(byteData), ac.SecretKey)
		if err != nil {
			return nil, err
		}
		bodyByte, err = json.Marshal(map[string]string{"data": bodyData})
		if err != nil {
			return nil, err
		}
	}

	bodyStr := string(bodyByte)
	sign := sign(signData, bodyStr, ac.SecretKey)
	buffer := bytes.NewBuffer(bodyByte)
	request, err := http.NewRequest(method, url, buffer)
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", "application/json;charset=UTF-8") //添加请求头
	request.Header.Set("appId", ac.AppID)
	request.Header.Set("bizId", ac.BizID)
	request.Header.Set("timestamps", milliTime)
	request.Header.Set("sign", sign)
	client := &http.Client{Timeout: 5 * time.Second}            //创建客户端
	resp, err := client.Do(request.WithContext(context.TODO())) //发送请求
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return respBytes, nil
}

/**
 * @title: aes128gcm
 * @description: "AES-128/GCM + BASE64 加密"
 * @param {string} originalText "待加密字符串"
 * @param {string} secretKey "密钥"
 * @return {string} "加密后字符串"
 * @return {error} "错误信息"
 */
func aes128gcm(originalText, secretKey string) (string, error) {
	// 密钥需要解码
	key, err := hex.DecodeString(secretKey)
	if err != nil {
		return "", err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	aesGcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	// 向量
	nonce := make([]byte, aesGcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}
	cipherText := aesGcm.Seal(nonce, nonce, []byte(originalText), nil)

	// encode as base64 string
	encoded := base64.StdEncoding.EncodeToString(cipherText)
	return encoded, nil
}

/**
 * @title: sign
 * @description: "计算签名"
 * @param {map[string]string} headers "header的参数"
 * @param {string} body "body参数"
 * @param {string} secretKey "密钥"
 * @return {string} "签名"
 */
func sign(headers map[string]string, body, secretKey string) string {
	var data string
	var keys []string
	// key排序
	for k := range headers {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	// 拼接
	for _, k := range keys {
		data = data + k + headers[k]
	}
	data = secretKey + data + body

	// 对字符串进行sha256哈希
	h := sha256.New()
	h.Write([]byte(data))
	sum := h.Sum(nil)
	return hex.EncodeToString(sum)
}
