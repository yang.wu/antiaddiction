package antiaddiction

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"testing"
	"time"
)

func TestCheck(t *testing.T) {
	appID := ""
	secretKey := ""
	bizId := ""
	name := ""
	idNum := ""
	w := md5.New()
	io.WriteString(w, name+"_"+idNum)
	ai := fmt.Sprintf("%x", w.Sum(nil))
	r, err := NewAnti(appID, secretKey, bizId).Check(ai, name, idNum)
	if err != nil {
		fmt.Println(err, "err")
		return
	}
	b, _ := json.Marshal(r)
	fmt.Println(string(b), err)
	// pi 1h54i51vmtc6zgoltaj3vvhin91ds3nb618id4
}

func TestQuery(t *testing.T) {
	appID := ""
	secretKey := ""
	bizId := ""
	name := ""
	idNum := ""
	w := md5.New()
	io.WriteString(w, name+"_"+idNum)
	ai := fmt.Sprintf("%x", w.Sum(nil))
	r, err := NewAnti(appID, secretKey, bizId).Query(ai)
	if err != nil {
		fmt.Println("err", err)
		return
	}
	b, _ := json.Marshal(r)
	fmt.Println(string(b), err)
}

func TestLoginout(t *testing.T) {
	appID := ""
	secretKey := ""
	bizId := ""
	a := Collection{
		No: 1,
		Si: "2",
		Bt: 0,
		Ot: time.Now().Unix(),
		Ct: 0,
		Di: "",
		Pi: "",
	}
	collections := []Collection{
		a,
	}
	r, err := NewAnti(appID, secretKey, bizId).Loginout(collections)
	if err != nil {
		fmt.Println(err, "err")
		return
	}
	b, _ := json.Marshal(r)
	fmt.Println(string(b), err)
}
