# Changelog

> * 在建立新的MR的时候，需要同时修改本文件，增加本MR的修改内容，同时迭代版本号。
> * MR根据其内容功能可划分为 Bug Fixes、Improvements 和 New Features。
> * 每一条MR需要说明其内容，以及附上MR编号和链接，具体格式如下。
## 1.x.x (2021-xx-xx) 


Bug Fixes
* Fix foo ([xxx](https://gitlab.com/yang.wu/antiaddiction/-/merge_requests/xxx))  

Improvements:
* Add bar ([xxx](https://gitlab.com/yang.wu/antiaddiction/-/merge_requests/xxx))

New Features:
* Support baz ([xxx](https://gitlab.com/yang.wu/antiaddiction/-/merge_requests/xxx))
* Enable qux ([xxx](https://gitlab.com/yang.wu/antiaddiction/-/merge_requests/xxx))
