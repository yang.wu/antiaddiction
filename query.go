package antiaddiction

import (
	"encoding/json"
	"net/http"
)

type QueryResult struct {
	Error
	Data *struct {
		Result struct {
			Status int64  `json:"status"`
			Pi     string `json:"pi"`
		} `json:"result"`
	} `json:"data,omitempty"`
}

/**
 * @title: Query
 * @description: "实名认证结果查询接口。网络游戏用户实名认证结果查询服务接口，面向已经提交用户实名认证且没有返回结果的游戏运营单位提供服务，游戏运营单位可以调用该接口，查询已经提交但未返回结果用户的实名认证结果。"
 * @param {string} ai "游戏内部成员标识"
 * @return {*QueryResult} "实名认证结果"
 * @return {error} "错误信息"
 */
func (ac *AntiConfig) Query(ai string) (*QueryResult, error) {
	url := queryURL + "ai=" + ai
	rbyte, err := ac.request(http.MethodGet, url, nil, map[string]string{"ai": ai})
	if err != nil {
		return nil, err
	}
	r := new(QueryResult)
	if err := json.Unmarshal(rbyte, r); err != nil {
		return nil, err
	}
	return r, nil
}
