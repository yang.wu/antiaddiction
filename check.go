package antiaddiction

import (
	"encoding/json"
	"net/http"
)

type CheckResult struct {
	Error
	Data *struct {
		Result struct {
			Status int64  `json:"status"`
			Pi     string `json:"pi"`
		} `json:"result"`
	} `json:"data,omitempty"`
}

/**
 * @title: Check
 * @description: "网络游戏用户实名认证服务接口，面向已经接入网络游戏防沉迷实名认证系统的游戏运营单位提供服务，游戏运营单位调用该接口进行用户实名认证工作，本版本仅支持大陆地区的姓名和二代身份证号核实认证。"
 * @param {string} ai "游戏内部成员标识，本次实名认证行为在游戏内部对应的唯一标识，该标识将作为实名认证结果查询的唯一依据备注：不同企业的游戏内部成员标识有不同的字段长度，对于超过32位的建议使用哈希算法压缩，不足32位的建议按企业自定规则补齐"
 * @param {string} name "用户姓名，游戏用户姓名（实名信息）"
 * @param {string} idNum "用户身份证号码，游戏用户身份证号码（实名信息）"
 * @return {*CheckResult} "实名认证结果"
 * @return {error} "错误信息"
 */
func (ac *AntiConfig) Check(ai, name, idNum string) (*CheckResult, error) {
	data := map[string]interface{}{
		"ai":    ai,
		"name":  name,
		"idNum": idNum,
	}
	rbyte, err := ac.request(http.MethodPost, checkURL, data, nil)
	if err != nil {
		return nil, err
	}
	r := new(CheckResult)
	if err := json.Unmarshal(rbyte, r); err != nil {
		return nil, err
	}
	return r, nil
}
