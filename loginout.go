package antiaddiction

import (
	"encoding/json"
	"net/http"
)

// 用户行为类型，游戏用户行为类型，0-下线、1-上线
type BtType int

const (
	BtLogout BtType = 0 // 下线
	BtLogin  BtType = 1 // 上线
)

// 上报类型，用户行为数据上报类型，0-已认证通过用户、2-游客用户
type CtType int

const (
	CtUser  CtType = 0 // 已认证通过用户
	CtGuest CtType = 2 // 游客用户
)

// LoginoutParam 用户行为数据上报请求参数
type LoginoutParam struct {
	Collections []*Collection `json:"collections"`
}

type Collection struct {
	No int8   `json:"no"` // 条目编码，在批量模式中标识一条行为数据，取值范围1-128
	Si string `json:"si"` // 游戏内部会话标识，一个会话标识只能对应唯一的实名用户，一个实名用户可以拥有多个会话标识；同一用户单次游戏会话中，上下线动作必须使用同一会话标识上报备注：会话标识仅标识一次用户会话，生命周期仅为一次上线和与之匹配的一次下线，不会对生命周期之外的任何业务有任何影响
	Bt BtType `json:"bt"` // 用户行为类型，游戏用户行为类型，0-下线、1-上线
	Ot int64  `json:"ot"` // 行为发生时间，行为发生时间戳，单位秒
	Ct CtType `json:"ct"` // 上报类型，用户行为数据上报类型，0-已认证通过用户、2-游客用户
	Di string `json:"di"` // 设备标识，游客模式设备标识，由游戏运营单位生成，游客用户下必填
	Pi string `json:"pi"` // 用户唯一标识，已通过实名认证用户的唯一标识，已认证通过用户必填
}

type LoginoutResult struct {
	Error
	Data *struct {
		Results []struct {
			No int8 `json:"no"`
			Error
		} `json:"results"`
	} `json:"data,omitempty"`
}

/**
 * @title: Loginout
 * @description: "游戏用户行为数据上报接口。游戏用户行为数据上报接口，面向已经接入网络游戏防沉迷实名认证系统的游戏运营单位提供服务，游戏运营单位调用该接口上报游戏用户上下线行为数据。"
 * @param {[]Collection} collections "游戏用户上下线行为数据上报对象"
 * @return {*LoginoutResult} "上报结果"
 * @return {error} "错误信息"
 */
func (ac *AntiConfig) Loginout(collections []Collection) (*LoginoutResult, error) {
	data := map[string]interface{}{
		"collections": collections,
	}
	rbyte, err := ac.request(http.MethodPost, loginoutURL, data, nil)
	if err != nil {
		return nil, err
	}
	r := new(LoginoutResult)
	if err := json.Unmarshal(rbyte, r); err != nil {
		return nil, err
	}
	return r, nil
}
